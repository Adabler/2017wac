#include <iostream>

using namespace std;

int main()
{
    long long int n;
    cin >> n;
    bool poprawne = true;
    if(n == 0){
      poprawne = false;
    }

    while(n != 0){
        if(n%10 != 4 && n%10 != 7){
            poprawne = false;
            break;
        }
        n /= 10;
    }

    if(poprawne){
        cout << "TAK";
    }
    else{
        cout << "NIE";
    }

    return 0;
}
