#include <iostream>

using namespace std;

int main()
{
    int t;
    cin >> t;
    long int a[t], wynik[t];

    for(int i=0; i < t; i++){
        cin >> a[i];
    }
    long int maximum = a[0];
    cout << maximum;
    for(int i=1; i < t; i++){
        if(maximum > a[i]){
            wynik[i] = maximum;
        }
        else{
            wynik[i] = a[i];
            maximum = a[i];
        }
        cout << endl << wynik[i];
    }
}
