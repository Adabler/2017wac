#include <iostream>

using namespace std;

int main()
{
   int n;
   cin >> n;

   bool strzaly[8][8];
   for(int i=0; i<8; i++){
       for(int j=0; j<8; j++){
           strzaly[i][j] = false;
       }
   }
   int wiersz, kolumna;

   for(int i=0; i<n; i++){
       cin >> wiersz;
       cin >> kolumna;
       strzaly[wiersz-1][kolumna-1] = true;
   }

   for(int i=0; i<8; i++){
       for(int j=0; j<8; j++){
           if(strzaly[i][j] == true){
               cout << "X";
           }
           else{
               cout << ".";
           }
       }
       cout << endl;
   }
}
