#include <iostream>

using namespace std;

string kropki(int ilosc){
    string kropki = "";
    for(int i=0; i<ilosc; i++){
        kropki += ".";
    }
    return kropki;
}
string krzyzyki(int ilosc){
    string krzyzyki = "";
    for(int i=0; i<ilosc; i++){
        krzyzyki += "X";
    }
    return krzyzyki;
}

int main()
{
    int n;
    cin >> n;


    for(int i=n%2; i<n; i += 2){
        if(i == 0){
            continue;
        }
        cout << kropki((n-i)/2) << krzyzyki(i) << kropki((n-i)/2) << endl;
    }
    for(int i=n; i>0; i -= 2){
        if(i == 0){
            continue;
        }
        if(n == i && i%2==0){
            cout << kropki((n-i)/2) << krzyzyki(i) << kropki((n-i)/2) << endl;
        }
        cout << kropki((n-i)/2) << krzyzyki(i) << kropki((n-i)/2) << endl;
    }

    cin.get();
    return 0;
}
