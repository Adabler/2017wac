#include <iostream>

using namespace std;

int nwd(int liczba, int liczba_2){

    do{
        if(liczba>liczba_2){
            liczba=liczba-liczba_2;
        }
        else{
            liczba_2=liczba_2-liczba;
        }
    }
    while(liczba!=liczba_2);

    return liczba;
}

int main()
{
    int n;
    cin >> n;
    int ilosc = 0;

    for(int i=n-1; i>0; i--){
        if(nwd(n, i) == 1){
            ilosc++;
        }
    }

    cout << ilosc;

    return 0;
}
