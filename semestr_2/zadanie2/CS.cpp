#include <iostream>

using namespace std;

int main()
{
    unsigned long long int n;
    cin >> n;

    if(n >= 20){
        cout << "0000";
        return 0;
    }

    unsigned long long int silnia = 1;

    for(int i=2;i<=n;i++){
        silnia *= i;
    }

    int wynik[4];
    int ilosc = -1;
    for(int i = 0; i < 4; i++){
        if(silnia == 0){
            break;
        }
        ilosc++;
        wynik[i] = silnia%10;
        silnia /= 10;
    }

    for(int i = ilosc; i >= 0; i--){
        cout << wynik[i];
    }

    return 0;
}
