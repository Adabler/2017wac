#include <iostream>

using namespace std;

int main()
{
    int n;
    cin >> n;
    int a[n], b[n];
    for(int i=0; i<n; i++){
        cin >> a[i];
        b[i] = a[i];
    }

    cout << b[0];
    for(int i=1; i<n; i++){
        b[i] += b[i-1];
        cout << " " << b[i];
    }

    return 0;
}
