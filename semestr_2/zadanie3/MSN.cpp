#include <iostream>

using namespace std;

unsigned long long int silnia(int liczba){
    if(liczba > 2){
        unsigned long long int wynik = 2;
        for(int i = 3; i <= liczba; i++){
            wynik = wynik * i;
        }
        return wynik;
    }
    else if(liczba > 0){
        return liczba;
    }
    return 1;
}

int main()
{
   unsigned int n, k;

   cin >> n >> k;

   unsigned long long int wynik = 0;

   wynik = (silnia(n) / (silnia(k)*(silnia(n-k))));

   cout << wynik;
}
