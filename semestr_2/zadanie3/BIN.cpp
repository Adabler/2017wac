#include <iostream>

using namespace std;

int main()
{
   unsigned long long int liczba;
   string wynik = "";

   cin >> liczba;

   while(liczba != 0){
       if(liczba%2 == 1){
           wynik = "1" + wynik;
       }
       else{
           wynik = "0" + wynik;
       }
       liczba = liczba / 2;
   }

   cout << wynik;
}
