#include <iostream>
#include <cstring>

using namespace std;

long long int arab2bin(const char * x){

    if(strlen(x) > 4){
        return -1;
    }
    else if(x[0] == '-'){
        return -2;
    }
    for(int i = 0; i < strlen(x); i++){
        if(i == 0){
            if(x[i] < '1' || x[i] > '9'){
                return 0;
            }
        }
        else{
            if(x[i] < '0' || x[i] > '9'){
                return 0;
            }
        }
    }

    int liczba = stoi(x);
    string sbin = "";
    while(liczba > 0){
        if(liczba%2 == 0){
            sbin = "0" + sbin;
        }
        else{
            sbin = "1" + sbin;
        }
        liczba = liczba/2;
    }
    long long int bin = stol(sbin);
    return bin;
}

string bin2rzym(long long int x){
    if(x <= 0){
        if(x == 0){
            return "Liczba niewłaściwa";
        }
        else if(x == -1){
            return "Liczba wieksza niż 9999";
        }
        else if(x == -2){
            return "Liczba ujemna";
        }
    }
    const int B[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    const string R[] = {"M","CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
    string result = "";
    for(int i = 0; i < sizeof(B)/sizeof(B[0]); i++){
        if(x >= B[i]){
            x -= B[i];
            result += R[i];
        }
    }
    return result;
}
int main()
{
    string liczba[200];

    for(int i = 0; cin; i++){
        cin>>liczba[i];
    }

    for(int i = 0; liczba[i] != ""; i++){
        long long int bin = arab2bin(liczba[i].c_str());
        cout<<liczba[i] << " - " << bin2rzym(bin)<<endl;
    }
}
