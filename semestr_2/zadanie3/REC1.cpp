#include <iostream>

using namespace std;

int ciag(int n){
    if(n == 0){
        return 2;
    }
    if(n % 2 == 0){
        return ciag(n/2)*ciag(n/2);
    }
    else{
        return ciag(n-1);
    }
}

int main()
{
   int n;
   cin >> n;
   cout << ciag(n);
}
