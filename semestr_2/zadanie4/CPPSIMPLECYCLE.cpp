#include <iostream>

using namespace std;

int main()
{
   int n;
   cin >> n;

   int x[n], current;

   for(int i=0; i<n; i++){
       cin >> x[i];
       if(x[i] == 0){
           current = i;
       }
   }

   do{
       cout << x[current] << " ";
       current = x[current];
   }
   while(x[current] != 0);
}
