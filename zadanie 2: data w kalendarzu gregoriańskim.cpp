#include <iostream>
#include <time.h>

using namespace std;

class data
{
private:
  int ile_dni[2][12] = {{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}, {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};
  int dzien, miesiac, rok;

  bool czyPrzestepny(unsigned rok){
    return (rok % 4 == 0 && rok % 100 != 0 || rok % 400 == 0);
  }


public:
  data(int d, int m, int y){
    if(y <= 0){
      throw std::invalid_argument("Niewłasciwy argument roku");
    }
    if(m < 1 || m > 12){
      throw std::invalid_argument("Niewłasciwy argument miesiaca");
    }
    if(d < 1 || d > this->ile_dni[czyPrzestepny(y)][m-1]){
      throw std::invalid_argument("Niewłasciwy argument dnia");
    }
    this->dzien = d;
    this->miesiac = m;
    this->rok = y;
  }
  data(){
    time_t t = time(0);
    struct tm * now = localtime(&t);
    this->dzien = now->tm_mday;
    this->miesiac = now->tm_mon + 1;
    this->rok = now->tm_year + 1900;
  }

  void poprzedni(){
    if(this->dzien == 1){
      if(this->miesiac == 1){
        this->rok--;
        this->miesiac = 12;
      }
      else{
        this->miesiac--;
      }
      this->dzien = ile_dni[czyPrzestepny(this->rok)][this->miesiac-1];
    }
    else{
      this->dzien--;
    }
  }
  void nastepny(){
    if(this->dzien == ile_dni[czyPrzestepny(this->rok)][this->miesiac-1]){
      if(this->miesiac == 12){
        this->rok++;
        this->miesiac = 1;
      }
      else{
        this->miesiac++;
      }
      this->dzien = 1;
    }
    else{
      this->dzien++;
    }
  }
};

int main(){
  int dzien = 0, miesiac = 0, rok = 0;

  cin>>dzien>>miesiac>>rok;

  data obiekt = data();
  // data obiekt = data(dzien, miesiac, rok);

  // obiekt.poprzedni();
  // obiekt.nastepny();
}
