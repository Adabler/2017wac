#include <iostream>

using namespace std;

int ciag(int A, int B, int n){
    if(n == 1){
        return A;
    }
    if(n % 2 == 0){
        return B*ciag(A, B, n/2);
    }
    else{
        return A+ciag(A, B, n-1);
    }
}

int main()
{
   int A, B, n;
   cin >> A >> B >> n;
   cout << ciag(A, B, n);
}
